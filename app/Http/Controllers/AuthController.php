<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitor;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{
    public function getLogin(){
        return view('auth.login');
    }

    public function postLogin(Request $request){
			$email = $request->email;															// get email data from login form
			$password = $request->password;												// get password data from login form
	
			$data = Visitor::where('email',$email)->first();	// get data from database by email
			if($data){	
                $check = Hash::check($password, $data->password);																			// validate an email is available or not in database
				if($check){							// validate an inputed password with a password data from database
					Session::put('name',$data->name);						// store name data with session
                    Session::put('email',$data->email);				// store email data with session  
                    // if(Input::get('remember_me', true)) 
                    //     return view('user.menu')->withCookie(Cookie::make('email', $request->email, 30*60*1000));
                    // else 
                    alert()->success('Login Success','Welcome to La-Travel !'); 
                    return view('user.menu');	       
			    } else {    
                    alert()->error('Login Failed','Email or Password is wrong!'); 
                    return redirect('login'); 									// return view login page if email or password is false
				}
			}else{
                alert()->error('Login Failed','Email or Password is wrong!');     
                return redirect('login');										// return view login page if data isn't available in database
					
			}
        
    }

    public function getRegister(){
        return view('auth.register');
    }

    public function postRegister(Request $request){
        $validator = Validator::make($request-> all(), [ 
            'name' => 'required|min:4',
            'email' => 'required|email|unique:visitors',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
            
        ]);
        if($validator->fails()) { 
            return redirect('/register')
             ->withInput() 
             ->withErrors($validator);
        }
        Visitor::create([
            'name' => $request->name,
            'email' => $request->email,
            'password'=> Hash::make($request->password),
            'gender' => $request->gender
        ]);
        
        $minutes = 60; 

        alert()->success('Register Success','Please Login.'); 
        return redirect()->route('login');
    }


    public function logout(Request $request){
        // \Auth::logout();
        Session::flush();
        // $request->cookie()->forget('email');
        alert()->success('Sign Out Success','You signed out successfully !'); 
        return redirect()->route('login');
    }

     
}
