@extends('layouts.app')
@section('content')
<div id="bg-profile" class="view">
    <div class="mask rgba-black-strong">
        <div class="container-fluid d-flex align-items-center justify-content-center h-100">
            <div class="row justify-content-center text-center">
                <div class="col-md-20 border border-light">
                    <div class="m-5">
                        <h1 class="text-white mb-3">EDIT PROFILE </h1>
                        <div>
                            <input type="text" id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Name" value="{{  Auth::user()->name }}" autofocus required>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif

                            <input type="email" id="defaultRegisterFormEmail" name="email" class="form-control mt-4 {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="E-mail" value="{{ Auth::user()->email }}" autofocus required>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif

                            <a href=""><button class="btn btn-info btn-block my-4"> Submit </button></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>